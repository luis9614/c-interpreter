#include <QCoreApplication>
#include <QFile>
#include <QDir>
#include <iostream>
#include <stdio.h>
#include <QTextStream>
#include <lexicanalyzer.h>
#include "sintaxanalyzer.h"

using namespace std;

int main()
{
    LexicAnalyzer an;
    SintaxAnalyzer sa;
    //Abre el archivo y lee el texto
    string route = QDir::homePath().toStdString() + "/test.txt";
    QFile* currentFile = new QFile(QString::fromStdString(route));

    if(currentFile!=nullptr){
        QString text = "";

        if(currentFile->open(QIODevice::ReadOnly | QIODevice::Text)){
            QTextStream in(currentFile);
            while(!in.atEnd()){
                text = text + in.readLine() + " ";
            }
            //cout<< text.toStdString();
            delete currentFile;
            an.AnalyzeCode(text.toStdString());
            an.printVector();

            sa.analizeSyntax(an.tokens,0,an.tokens.size());

        }else{
            cout << "No file could be read";
        }
    }else{
        cout<<"No se encontró el archivo";
    }



    return 0;

}
