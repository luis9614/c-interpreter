#ifndef EXPRESSION_H
#define EXPRESSION_H
#include "token.h"
#include <vector>

class Expression
{
public:
    Expression();
    vector<Token> expression_parts;

    void evaluate();
};

#endif // EXPRESSION_H
