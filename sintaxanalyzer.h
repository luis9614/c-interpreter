#ifndef SINTAXANALYZER_H
#define SINTAXANALYZER_H
#include <vector>
#include "instruction.h"
#include "token.h"


class SintaxAnalyzer
{
public:
    SintaxAnalyzer();
    vector<Instruction> analyzer;

    void analizeSyntax(vector<Token> &,int,int);
};

#endif // SINTAXANALYZER_H
