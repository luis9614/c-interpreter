#ifndef ASIGNATION_H
#define ASIGNATION_H
#include "instruction.h"
#include "token.h"

class Asignation : public Instruction
{
public:
    Asignation();

    Token identifier;
    //Expression expression;

    void execute();
    void print();
};

#endif // ASIGNATION_H
