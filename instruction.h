#ifndef INSTRUCTION_H
#define INSTRUCTION_H


class Instruction
{
public:
    Instruction();

    virtual void execute();
    virtual void print();
};

#endif // INSTRUCTION_H
