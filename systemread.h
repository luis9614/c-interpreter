#ifndef SYSTEMREAD_H
#define SYSTEMREAD_H
#include "token.h"
#include "instruction.h"


class SystemRead : public Instruction
{
public:
    SystemRead();

    Token identifier;

    void execute();
};

#endif // SYSTEMREAD_H
