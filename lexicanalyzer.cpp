#include "lexicanalyzer.h"
#include "token.h"
#include "string"
#include <QString>
#include <QStringList>
#include <iostream>

using namespace std;

LexicAnalyzer::LexicAnalyzer()
{   //Tipos de Datos
    dataTypes.insert(make_pair("number",20));
    dataTypes.insert(make_pair("decimal",21));
    dataTypes.insert(make_pair("text",22));
    dataTypes.insert(make_pair("boolean",23));
    dataTypes.insert(make_pair("true",24));
    dataTypes.insert(make_pair("false",25));

    //Estructuras de Control
    controlStructures.insert(make_pair("if",41));
    controlStructures.insert(make_pair("else",42));
    controlStructures.insert(make_pair("switch",43));
    controlStructures.insert(make_pair("for",44));
    controlStructures.insert(make_pair("while",45));
    controlStructures.insert(make_pair("print",46));
    controlStructures.insert(make_pair("read",47));
}

void LexicAnalyzer::AnalyzeCode(string code)
{
    Token auxToken;
    int token_type = 0;
    string aux;
    char auxChar;

    QString text = QString::fromStdString(code);
    QStringList parts = text.split(" ",QString::SkipEmptyParts);
    //cout<<parts.size();
    cout<<endl;
    for(int i = 0 ; i < parts.size() ; i++) {
        //cout<< parts.at(i).toStdString() <<endl;

        aux = parts.at(i).toStdString();
        auxChar = aux.at(0);
        token_type = 0;

        if( (auxChar<=122 && auxChar>=97) || (auxChar<=90 && auxChar>=65) || auxChar == 95){
            token_type = identifierOrReservedWord(aux);
            auxToken = Token(aux,token_type);
            this->tokens.push_back(auxToken);

        }else if (auxChar<=57 && auxChar>=48){
            token_type = numberOrDecimal(aux);
            auxToken = Token(aux,token_type);
            this->tokens.push_back(auxToken);
            //Llamar método que determine si es entero o flotante
        }else if(auxChar == 58){
            token_type = 9;
            auxToken = Token(aux,token_type);
            this->tokens.push_back(auxToken);

            //Asignación
        }else if(auxChar == 123 || auxChar == 125 || auxChar == 59){
            token_type = 5;
            auxToken = Token(aux,token_type);
            this->tokens.push_back(auxToken);
        }else if(auxChar == 38 || auxChar == 124 || auxChar == 33){
            token_type = 6;
            auxToken = Token(aux,token_type);
            this->tokens.push_back(auxToken);
        }else if(auxChar == 43 || auxChar == 45 || auxChar == 42 || auxChar == 47){
            token_type = 7;
            auxToken = Token(aux,token_type);
            this->tokens.push_back(auxToken);
        } else if(auxChar == 62 || auxChar == 60 || auxChar == 61 || auxChar == 35){
            token_type = 8;
            auxToken = Token(aux,token_type);
            this->tokens.push_back(auxToken);
        }

    }


}

int LexicAnalyzer::identifierOrReservedWord(string word)
{
    if(this->controlStructures.find(word)!=controlStructures.end()){
        return this->controlStructures.at(word);
    }else if(this->dataTypes.find(word)!=this->dataTypes.end()){
        return this->dataTypes.at(word);
    }else{
        return 1;
    }
}

int LexicAnalyzer::numberOrDecimal(string number)
{
    bool hasLetters;
    int cont = 0;

    for(int i=0;i<number.length();i++){
        if(number.at(i) == '.'){
            cont++;
        }else if(number.at(i)<48 || number.at(i)>57){
            return -1;
        }
    }
    if(cont==0){
        return 20;
    }else if(cont == 1){
        return 21;
    }else{
        return -1;
    }
}

void LexicAnalyzer::printVector()
{
    for(int i=0;i<this->tokens.size();i++){
        cout << tokens.at(i).tostring()<<endl;
    }
}
