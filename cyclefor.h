#ifndef CYCLEFOR_H
#define CYCLEFOR_H
#include "instruction.h"
#include "token.h"
#include "vector"


class CycleFOR
{
public:
    CycleFOR();
    Token identifier;
    int beginning;
    int increment;
    int end;
    vector<Instruction> tasks;

    void execute();
};

#endif // CYCLEFOR_H
