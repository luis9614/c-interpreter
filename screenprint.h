#ifndef SCREENPRINT_H
#define SCREENPRINT_H
#include "instruction.h"
#include "expression.h"


class ScreenPrint : Instruction
{
public:
    ScreenPrint();
    Expression expression;

    void execute();
    void print();
};

#endif // SCREENPRINT_H
