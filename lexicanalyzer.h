#ifndef LEXICANALYZER_H
#define LEXICANALYZER_H
#include <vector>
#include <token.h>
#include <map>


class LexicAnalyzer
{
private:
    map<string,int> controlStructures;
    map<string,int> dataTypes;
public:
    LexicAnalyzer();
    vector<Token> tokens;
    void AnalyzeCode(string);

    int identifierOrReservedWord(string);
    int numberOrDecimal(string);

    void printVector();
};

#endif // LEXICANALYZER_H
