#ifndef TOKEN_H
#define TOKEN_H
#include <string>

using namespace std;

class Token
{
public:
    Token();
    Token(string, int);

    string token;
    int token_type;

    string tostring();
    //Falta Implementar
        int getTokenClass();
};

#endif // TOKEN_H
