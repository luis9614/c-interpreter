#ifndef CYCLEIF_H
#define CYCLEIF_H
#include "expression.h"
#include "instruction.h"
#include "vector"



class CycleIF
{
public:
    CycleIF();
    vector<Instruction> case_if;
    vector<Instruction> case_else;
    Expression expression;

    void execute();
};

#endif // CYCLEIF_H
