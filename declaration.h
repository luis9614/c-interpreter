#ifndef DECLARATION_H
#define DECLARATION_H
#include "instruction.h"
#include "token.h"


class Declaration /*: public Instruction*/
{
public:
    Declaration();

    Token data_type;
    Token identifier;

    void execute();
};

#endif // DECLARATION_H
